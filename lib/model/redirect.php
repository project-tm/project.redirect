<?php

namespace Project\Redirect\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main,
    Project\Redirect\Config;

class RedirectTable extends DataManager {

    public static function tableCreate() {
        static::getEntity()->getConnection()->query("CREATE TABLE IF NOT EXISTS " . self::getTableName() . " (
            ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
            URL VARCHAR(255),
            TYPE VARCHAR(50),
            NEW_URL VARCHAR(255),
            ELEMENT INT,
            PARAM1 INT
        );");
    }

    public static function tableDrop() {
        if (Config::IS_FROP_TABLE) {
            static::getEntity()->getConnection()->query("DROP TABLE IF EXISTS " . self::getTableName() . ";");
        }
    }

    public static function getTableName() {
        return 'project_redirect';
    }

    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\StringField('URL', array(
                'title' => 'Адрес'
                    )),
            new Main\Entity\StringField('TYPE', array(
                'title' => 'Тип'
                    )),
            new Main\Entity\StringField('NEW_URL', array(
                'title' => 'Новый адрес'
                    )),
            new Main\Entity\IntegerField('ELEMENT', array(
                'title' => 'ELEMENT_ID'
                    )),
            new Main\Entity\IntegerField('PARAM1', array(
                'title' => 'Параметр 1'
                    ))
        );
    }

}
