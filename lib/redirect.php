<?php

namespace Project\Redirect;

use CDBResult,
    Project\Redirect\Model\RedirectTable;

class Redirect {

    static public function add($url, $type, $id, $param1 = 0) {
        $rsData = RedirectTable::getList(array(
                    "select" => array('ID'),
                    'filter' => array('URL' => $url)
        ));
        $rsData = new CDBResult($rsData);
        if ($arItem = $rsData->Fetch()) {
            $arFields = array(
                'TYPE' => $type,
                'ELEMENT' => $id,
                'PARAM1' => $param1
            );
            RedirectTable::update($arItem['ID'], $arFields);
        } else {
            $arFields = array(
                'URL' => $url,
                'TYPE' => $type,
                'ELEMENT' => $id,
                'PARAM1' => $param1
            );
            RedirectTable::add($arFields);
        }
    }

}
