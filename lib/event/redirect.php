<?

namespace Project\Redirect\Event;

use CDBResult,
    CIBlockElement,
    CIBlockSection,
    Bitrix\Main\Loader,
    Project\Redirect\Config,
    Project\Redirect\Model\RedirectTable;

class Redirect {

    public static function toUrl($url) {
        if ($url != $_SERVER['REQUEST_URI']) {
            LocalRedirect($url, false, '301 Moved permanently');
        }
    }

    public static function OnPageStart() {
        if (!empty($_SERVER['REQUEST_URI']) and Loader::includeModule('iblock')) {
            $filter = array('=URL' => array(
                    $_SERVER['REQUEST_URI']
            ));
            if (substr($_SERVER['REQUEST_URI'], -1) == '/') {
                $filter['=URL'][] = substr($_SERVER['REQUEST_URI'], 0, -1);
            }
            $rsData = RedirectTable::getList(array(
                        'select' => array('TYPE', 'NEW_URL', 'ELEMENT', 'PARAM1'),
                        'filter' => $filter
            ));
            if ($arItem = $rsData->Fetch()) {
                if (!empty($arItem['NEW_URL'])) {
                    self::toUrl($arItem['NEW_URL']);
                }
                switch ($arItem['TYPE']) {
                    case 'SECTION':
                        $arFilter = Array(
                            'IBLOCK_ID' => Config::CATALOG_ID,
                            'ID' => $arItem['ELEMENT']
                        );
                        $res = CIBlockSection::GetList(array(), $arFilter, false, array('SECTION_PAGE_URL '));
                        if ($arItem = $res->GetNext()) {
                            self::toUrl($arItem['SECTION_PAGE_URL']);
                        }
                        break;

                    case 'ARTICLES':
                    case 'CATALOG':
                        $arSelect = Array(
                            'DETAIL_PAGE_URL',
                        );
                        $arFilter = Array(
                            'IBLOCK_ID' => Config::TYPE[$arItem['TYPE']],
                            'ID' => $arItem['ELEMENT']
                        );
                        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
                        if ($arItem = $res->GetNext()) {
                            self::toUrl($arItem['DETAIL_PAGE_URL']);
                        }
                        break;

                    case 'OFFERS':
                        $arSelect = Array(
                            'DETAIL_PAGE_URL',
                        );
                        $arFilter = Array(
                            'IBLOCK_ID' => Config::TYPE['CATALOG'],
                            'ID' => CIBlockElement::SubQuery('PROPERTY_CML2_LINK', array(
                                'IBLOCK_ID' => Config::TYPE[$arItem['TYPE']],
                                'ID' => $arItem['ELEMENT']
                            ))
                        );
                        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
                        if ($arItem = $res->GetNext()) {
                            self::toUrl($arItem['DETAIL_PAGE_URL']);
                        }
                        break;

                    default:
                        break;
                }
            }
//            str
//            preExit($_SERVER['REQUEST_URI']);
        }
    }

}
