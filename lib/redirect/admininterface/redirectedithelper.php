<?php

namespace Project\Redirect\Redirect\AdminInterface;

use Bitrix\Main\Localization\Loc;
use DigitalWand\AdminHelper\Helper\AdminEditHelper;

Loc::loadMessages(__FILE__);

/**
 * Хелпер описывает интерфейс, выводящий форму редактирования новости.
 *
 * {@inheritdoc}
 */
class RedirectEditHelper extends AdminEditHelper {

    protected static $model = 'Project\Redirect\Model\RedirectTable';

    /**
     * @inheritdoc
     */
    public function setTitle($title) {
        if (!empty($this->data)) {
            $title = Loc::getMessage('PROJECT_REDIRECT_R_EDIT_TITLE', array('#ID#' => $this->data[$this->pk()]));
        } else {
            $title = Loc::getMessage('PROJECT_REDIRECT_R_NEW_TITLE');
        }

        parent::setTitle($title);
    }

}
