<?php

namespace Project\Redirect\Redirect\AdminInterface;

use Bitrix\Main\Localization\Loc,
    DigitalWand\AdminHelper\Helper\AdminInterface,
    DigitalWand\AdminHelper\Widget;

Loc::loadMessages(__FILE__);

/**
 * Описание интерфейса (табок и полей) админки новостей.
 *
 * {@inheritdoc}
 */
class RedirectAdminInterface extends AdminInterface {

    /**
     * @inheritdoc
     */
    public function fields() {
        return array(
            'MAIN' => array(
                'NAME' => Loc::getMessage('PROJECT_REDIRECT_R_TAB_TITLE'),
                'FIELDS' => array(
                    'ID' => array(
                        'WIDGET' => new Widget\NumberWidget(),
                        'READONLY' => true,
                        'FILTER' => true,
                        'HIDE_WHEN_CREATE' => true
                    ),
                    'URL' => array(
                        'WIDGET' => new Widget\StringWidget(),
                        'SIZE' => 80,
                        'FILTER' => '%',
                        'REQUIRED' => true
                    ),
                    'TYPE' => array(
                        'WIDGET' => new Widget\StringWidget(),
                        'SIZE' => 80,
                        'REQUIRED' => true
                    ),
                    'NEW_URL' => array(
                        'WIDGET' => new Widget\StringWidget(),
                        'SIZE' => 80,
                        'FILTER' => '%',
                        'REQUIRED' => true
                    ),
                    'ELEMENT' => array(
                        'WIDGET' => new Widget\NumberWidget(),
                        'REQUIRED' => true
                    ),
                    'PARAM1' => array(
                        'WIDGET' => new Widget\NumberWidget(),
                        'REQUIRED' => true
                    )
                )
            )
        );
    }

    /**
     * @inheritdoc
     */
    public function helpers() {
        return array(
            '\Project\Redirect\Redirect\AdminInterface\RedirectListHelper' => array(
                'BUTTONS' => array(
                    'LIST_CREATE_NEW' => array(
                        'TEXT' => Loc::getMessage('PROJECT_REDIRECT_R_BUTTON_ADD_NEWS'),
                    )
                )
            ),
            '\Project\Redirect\Redirect\AdminInterface\RedirectEditHelper' => array(
                'BUTTONS' => array(
                    'ADD_ELEMENT' => array(
                        'TEXT' => Loc::getMessage('PROJECT_REDIRECT_R_BUTTON_ADD_NEWS')
                    ),
                    'DELETE_ELEMENT' => array(
                        'TEXT' => Loc::getMessage('PROJECT_REDIRECT_R_BUTTON_DELETE')
                    )
                )
            )
        );
    }

}
