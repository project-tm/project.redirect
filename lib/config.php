<?php

namespace Project\Redirect;

use Bitrix\Main\Loader;

Loader::includeModule('tm.opticsite');

class Config {

    const CATALOG_ID = \Tm\Opticsite\Iblock\Config::CATALOG_ID;
    const IS_FROP_TABLE = false;
    const TYPE = array(
        'PRODUCT' => \Tm\Opticsite\Iblock\Config::CATALOG_ID,
        'OFFERS' => \Tm\Opticsite\Iblock\Config::OFFERS_ID,
        'ARTICLES' => \Tm\Opticsite\Iblock\Config::ARTICLES,
    );

}
